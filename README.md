# Connection Test

This is a tiny little program made for quickly testing networking.  Specifically, it's made for testing to make sure that a computer can and has been successfully configured to allow incoming network connections to a webserver.

This was written with Foundry VTT in mind, hence the default port of `30000`, but it is ultimately a generic network test.  

## Usage

### Windows

Download the latest `connection_test.exe` and run it (Windows will complain about it being an unsigned program, you can More Info -> Run Anyways; I haven't looked into figuring out code signing yet).  At which point you should be able to access a webpage via http://localhost:30000 or the public IP link that it will generally show in the window that comes up when it runs.  

### Other OSes (or Windows users with Python installed)

Download the latest `connection_test.py` and run it.  It should behave the same as the Windows version (and most Linux or Mac machines should have Python installed by default to run `.py` files).