from http.server import SimpleHTTPRequestHandler, HTTPServer
import argparse

parser = argparse.ArgumentParser(prog='serve', description='A simple program for testing that a computer has an open port for hosting')
parser.add_argument('-p', '--port', default=30000, type=int)
args = parser.parse_args()

html_response = f'''
<html>
    <head>
        <title>Connection Test</title>
    </head>
    <body>
        Connection successful, port {args.port} is open and accepting HTTP connections.
    </body>
</html>
'''

# Minor tweak to the handler in order to respond with a hardcoded string to all GET requests
class CustomHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        """Serve a GET request."""
        f = self.send_head()
        if f:
            try: self.wfile.write(html_response.encode()) # Encode to bytestring for the socket write
            finally: f.close()

# Attempt to determine the public IP of the host machine and create a link to offer the user
try:
    from urllib import request
    public_ip = request.urlopen('http://4.ident.me').read().decode()
    print(f'Hosting webserver.  Your external IP appears to be {public_ip} and you can test connecting to http://{public_ip}:{args.port}')
except: pass

httpd = HTTPServer(('', args.port), CustomHandler)
httpd.serve_forever()